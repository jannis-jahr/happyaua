package de.happyhour;
import java.sql.ResultSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.mysql.jdbc.Statement;




public class Main {
    public static void main(String[] args) {
        // Open a database connection
        // (create a new database if it doesn't exist yet):
        EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("PersistenceUnit");
        EntityManager em = emf.createEntityManager();
        
      
 
        /*
        // Store 200 Veranstaltung objects in the database:
        em.getTransaction().begin();
        for (int i = 0; i < 200; i++) {
            Veranstaltung p = new Veranstaltung();
            em.persist(p);
        }
        em.getTransaction().commit();
 
 
        // Close the database connection:
        em.close();
        emf.close();
        */
    }
}