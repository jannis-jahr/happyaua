package de.happyhour;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;




public class Main {
    public static void main(String[] args) {
        // Open a database connection
        // (create a new database if it doesn't exist yet):
    	
        EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("PersistenceUnit");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        Veranstaltung veranstaltung = em.find(Veranstaltung.class, 1l);
        //Group group = em.find(Group.class, "LUG");
        tx.commit();
		
		em.close();
		emf.close();
		


 /*
        
        // Store 200 Veranstaltung objects in the database:
        em.getTransaction().begin();
        for (int i = 0; i < 200; i++) {
            Veranstaltung p = new Veranstaltung();
            em.persist(p);
        }
        em.getTransaction().commit();
 
 
        // Close the database connection:
        em.close();
        emf.close();
        */
        
    }
}